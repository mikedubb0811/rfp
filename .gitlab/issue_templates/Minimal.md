* [ ] The app complies with the [inclusion criteria](https://f-droid.org/wiki/page/Inclusion_Policy)
* [ ] The app is not already listed in the repo or issue tracker.
* [ ] The original app author has been notified (and does not oppose the inclusion).
* [ ] Optionally [donated](https://f-droid.org/donate/) to support the maintenance of this app in F-Droid.

---------------------

### Link to the source code:


### License used:


### Category:


### Summary:


### Description:

